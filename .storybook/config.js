import { configure, setAddon } from '@kadira/storybook';
import IntlAddon from 'react-storybook-addon-intl';
import '@blueprintjs/core/dist/blueprint.css';

setAddon(IntlAddon)

function loadStories() {
  require('../stories');
}

configure(loadStories, module);
