/**
*
* Drawer
*
*/

import React from 'react';
import Dock from 'react-dock';

class Drawer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Dock position="left" isVisible={this.props.open}>
        {this.props.children}
      </Dock>
    );
  }
}

Drawer.propTypes = {
  open: React.PropTypes.Boolean,
  children: React.PropTypes.node,
};

export default Drawer;
