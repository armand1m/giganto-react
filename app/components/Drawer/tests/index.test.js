import Drawer from '../index';

import expect from 'expect';
import { shallow } from 'enzyme';
import React from 'react';

describe('<Drawer />', () => {
  it('should render a <Dock> component');

  it('should render children', () => {
    const children = (<h1>Teste</h1>);

    const rendered = shallow(
      <Drawer>
        { children }
      </Drawer>
    );

    expect(rendered.contains(children)).toEqual(true);
  });
});
