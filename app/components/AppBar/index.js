/**
*
* AppBar
*
*/

import React from 'react';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import styled from 'styled-components';

const Marginless = styled.div`
  margin: 0 auto;
  width: 480px;
`;

class AppBar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <nav className="pt-navbar pt-dark">
        <Marginless>
          <div className="pt-navbar-group pt-align-left">
            <div className="pt-navbar-heading">
              <FormattedMessage {...messages.header} />
            </div>
          </div>
          <div className="pt-navbar-group pt-align-right">
            <span className="pt-navbar-divider"></span>
            <button className="pt-button pt-minimal pt-icon-user"></button>
            <button className="pt-button pt-minimal pt-icon-notifications"></button>
            <button className="pt-button pt-minimal pt-icon-cog"></button>
          </div>
        </Marginless>
      </nav>
    );
  }
}

export default AppBar;
