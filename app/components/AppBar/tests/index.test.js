import React from 'react';

import AppBar from '../index';

import expect from 'expect';
import { shallow } from 'enzyme';

describe('<AppBar />', () => {
  it('renders a <nav> with .pt-navbar', () => {
    const rendered = shallow(<AppBar />);

    expect(rendered.find('nav.pt-navbar').node).toExist();
  });

  it('has a left-side container', () => {
    const rendered = shallow(<AppBar />);
    expect(rendered.find('.pt-navbar-group.pt-align-left').node).toExist();
  });

  it('has a right-side container', () => {
    const rendered = shallow(<AppBar />);

    expect(rendered.find('.pt-navbar-group.pt-align-right').node).toExist();
  });

  it('has three buttons with the respective icons', () => {
    const icons = [
      'pt-icon-user',
      'pt-icon-notifications',
      'pt-icon-cog',
    ];

    const rendered = shallow(<AppBar />);

    icons.forEach((icon) => {
      expect(rendered.find(`.pt-button.${icon}`).node).toExist();
    });
  });
});
