import React from 'react';
import { storiesOf } from '@kadira/storybook';
import HomePage from '../app/containers/HomePage';
import AppBar from '../app/components/AppBar';
import Drawer from '../app/components/Drawer';
import { Menu, MenuItem, MenuDivider } from '@blueprintjs/core';

import { addLocaleData } from 'react-intl';
import pt from 'react-intl/locale-data/pt';

addLocaleData(pt);

storiesOf('AppBar', module)
  .addWithIntl('Normal state', () => (<AppBar />), {
    pt: {
      'app.components.AppBar.header': 'Giganto',
    },
  });

storiesOf('Drawer', module)
  .add('Opened', () => (<Drawer open />))
  .add('Opened with children', () => (
    <Drawer open>
      <h1>Teste</h1>
    </Drawer>
  ))
  .add('Opened with menu as children', () => (
    <Drawer open>
      <Menu>
        <MenuItem
          iconName="new-text-box"
          text="New text box"
        />
        <MenuItem
          iconName="new-object"
          text="New object"
        />
        <MenuItem
          iconName="new-link"
          text="New link"
        />
        <MenuDivider />
        <MenuItem
          text="Settings..."
          iconName="cog"
        />
      </Menu>
    </Drawer>
  ))
  .add('Closed', () => (<Drawer open={false} />));

storiesOf('HomePage', module)
  .addWithIntl('Normal state', () => (<HomePage />), {
    pt: {
      'app.components.HomePage.header': 'Este eh o componente da HomePage',
    },
  }, {
    initialLocale: 'en',
  });
